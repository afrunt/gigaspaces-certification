package com.afrunt.gs.feeder;

import com.afrunt.gs.model.Post;
import com.afrunt.gs.model.Posts;
import com.afrunt.gs.service.PostEngineService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openspaces.remoting.EventDrivenProxy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Andrey Frunt
 */
@Component
public class PostFeeder {
    private final Log log = LogFactory.getLog(PostFeeder.class);
    @EventDrivenProxy(timeout = 15000)
    private PostEngineService postEngineService;
    private int threadCount = 1;
    private int sleepTime = 100;

    @PostConstruct
    public void init() throws JAXBException {
        String feederSleepTime = System.getProperty("feederSleepTime");
        String feederThreadCount = System.getProperty("feederThreadCount");

        if (feederSleepTime != null) {
            sleepTime = Integer.valueOf(feederSleepTime);
        }

        if (feederThreadCount != null) {
            threadCount = Integer.valueOf(feederThreadCount);
        }

        InputStream xmlStream = PostFeeder.class.getClassLoader().getResourceAsStream("posts.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Posts.class, Post.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        Posts posts = (Posts) unmarshaller.unmarshal(xmlStream);
        log.info("ThreadCount=" + threadCount + " SleepTime=" + sleepTime + "ms");
        log.info("The number of posts is " + posts.getPosts().size());

        IdGenerator idGenerator = new IdGenerator();
        ExecutorService es = Executors.newFixedThreadPool(threadCount);
        for (int i = 0; i < threadCount; i++) {
            es.submit(new FeederThread(postEngineService, jaxbContext, posts.getPosts(), idGenerator, sleepTime));
        }
    }

    private static class FeederThread implements Runnable {
        private final Log log = LogFactory.getLog(FeederThread.class);
        private List<Post> posts;
        private IdGenerator idGenerator;
        private int sleepTime;
        private PostEngineService postEngineService;
        private Marshaller marshaller;

        public FeederThread(PostEngineService postEngineService, JAXBContext context, List<Post> posts, IdGenerator idGenerator, int sleepTime) throws JAXBException {
            this.postEngineService = postEngineService;
            this.marshaller = context.createMarshaller();
            this.posts = posts;
            this.idGenerator = idGenerator;
            this.sleepTime = sleepTime;
        }

        @Override
        public void run() {
            Random r = new Random(Thread.currentThread().getId());
            int postsCount = posts.size();
            while (true) {
                try {
                    int postIndex = generatePostIndex(r, postsCount);
                    Post post = posts.get(postIndex);
                    long id = idGenerator.generate();

                    log.info("Thread=" + Thread.currentThread().getId() + " Index=" + postIndex + " ID=" + id + " " + post.getUsername() + " " + post.getText());

                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    marshaller.marshal(post, bao);
                    postEngineService.submit(id, bao.toString());
                    Thread.sleep(sleepTime);
                } catch (Exception e) {
                    log.error(e);
                    e.printStackTrace();
                }
            }
        }

        private int generatePostIndex(Random r, int postsCount) {
            int nextInt = r.nextInt();
            return (nextInt > 0 ? nextInt : -nextInt) % postsCount;
        }
    }


    private static class IdGenerator {
        private long id = 0L;

        public synchronized long generate() {
            return id++;
        }
    }
}
