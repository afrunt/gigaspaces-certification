package com.afrunt.gs.service;

import org.openspaces.remoting.Routing;

/**
 * @author Andrey Frunt
 */
public interface PostEngineService {
    void submit(@Routing Long id, String post);
}
