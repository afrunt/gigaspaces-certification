package com.afrunt.gs.service;

import com.afrunt.gs.model.Post;

/**
 * @author Andrey Frunt
 */
public interface RemotePostService {
    Post[] findTop10Posts();

    Post[] findTop10PostsByUsername(String username);

    Post[] findTop10PostsByKeyword(String keyword);

    void removeOutdatedPosts();
}
