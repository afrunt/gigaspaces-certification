package com.afrunt.gs.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Posts {
    private List<Post> posts = new ArrayList<Post>();

    @XmlElement(name = "post")
    public List<Post> getPosts() {
        return posts;
    }
}
