package com.afrunt.gs.service;

import com.afrunt.gs.model.Post;
import com.afrunt.gs.reducer.Top10PostsReducer;
import org.openspaces.remoting.ExecutorProxy;
import org.springframework.stereotype.Component;

/**
 * @author Andrey Frunt
 */
@Component
public class PostService {
    @ExecutorProxy(broadcast = true, remoteResultReducerType = Top10PostsReducer.class)
    private RemotePostService service;

    public Post[] findTop10Posts() {
        return service.findTop10Posts();
    }

    public Post[] findPostsByUsername(String username) {
        return service.findTop10PostsByUsername(username);
    }

    public Post[] findPostsByKeyword(String keyword) {
        return service.findTop10PostsByKeyword(keyword);
    }
}
