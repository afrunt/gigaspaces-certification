package com.afrunt.gs.task;

import com.afrunt.gs.service.RemotePostService;
import org.openspaces.remoting.ExecutorProxy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author Andrey Frunt
 */
@Component
public class RemoveOldPostsTask {
    // Executor proxy used here to ensure that all space instances will remove the outdated posts
    @ExecutorProxy(broadcast = true)
    private RemotePostService remotePostService;

    /**
     * Sends request to the remote event-driven service every 2 minutes.
     */
    @Scheduled(cron = "0 0/2 * * * ?")
    public void run() {
        remotePostService.removeOutdatedPosts();
    }
}
