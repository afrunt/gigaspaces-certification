package com.afrunt.gs.reducer;

import com.afrunt.gs.model.Post;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openspaces.remoting.RemoteResultReducer;
import org.openspaces.remoting.SpaceRemotingInvocation;
import org.openspaces.remoting.SpaceRemotingResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Andrey Frunt
 */
public class Top10PostsReducer implements RemoteResultReducer<Post[], Post[]> {
    private final Log log = LogFactory.getLog(Top10PostsReducer.class);

    @Override
    public Post[] reduce(SpaceRemotingResult<Post[]>[] spaceRemotingResults, SpaceRemotingInvocation spaceRemotingInvocation) throws Exception {
        List<Post> posts = new ArrayList<Post>();
        for (SpaceRemotingResult<Post[]> result : spaceRemotingResults) {
            if (result.getException() != null) {
                log.error(result.getException());
                System.err.println(result.getException().getMessage());
                continue;
            }
            Collections.addAll(posts, result.getResult());
        }

        Collections.sort(posts, new Comparator<Post>() {
            public int compare(Post p1, Post p2) {
                return p2.getPublicationDate().compareTo(p1.getPublicationDate());
            }
        });

        Post[] result;

        if (posts.size() < 10) {
            result = posts.toArray(new Post[posts.size()]);
        } else {
            result = (posts.subList(0, 10)).toArray(new Post[10]);
        }

        return result;
    }
}
