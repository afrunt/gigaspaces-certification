package com.afrunt.gs.web.controller;

import com.afrunt.gs.service.PostService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import javax.annotation.Resource;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/")
public class ApplicationController {
    @Resource
    private PostService postService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String showTop(Model uiModel) {
        uiModel.addAttribute("posts", postService.findTop10Posts());
        uiModel.addAttribute("title", "Top 10");
        uiModel.addAttribute("currentPlace", "top10");
        return "searchResults";
    }

    @RequestMapping(value = {"/search", "search"}, method = RequestMethod.GET)
    public String showSearch(Model uiModel) {
        uiModel.addAttribute("title", "Search");
        uiModel.addAttribute("currentPlace", "search");
        return "search";
    }

    @RequestMapping(value = {"/search", "/search/"}, method = RequestMethod.POST)
    public String showSearchResults(Model uiModel, WebRequest request) {
        String keyword = request.getParameter("keyword");
        String username = request.getParameter("username");
        uiModel.addAttribute("currentPlace", "search");

        if (username != null) {
            uiModel.addAttribute("posts", postService.findPostsByUsername(username));
            uiModel.addAttribute("title", "Posts for username " + username);
        } else if (keyword != null) {
            uiModel.addAttribute("posts", postService.findPostsByKeyword(keyword));
            uiModel.addAttribute("title", "Posts for keyword " + keyword);
        } else {
            uiModel.asMap().clear();
            return "redirect:/search";
        }

        return "searchResults";
    }
}
