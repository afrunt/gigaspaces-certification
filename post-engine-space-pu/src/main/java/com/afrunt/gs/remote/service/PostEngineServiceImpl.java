package com.afrunt.gs.remote.service;

import com.afrunt.gs.model.Post;
import com.afrunt.gs.service.PostEngineService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openspaces.core.GigaSpace;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.Date;

/**
 * @author Andrey Frunt
 */
public class PostEngineServiceImpl implements PostEngineService {
    private final Log log = LogFactory.getLog(PostEngineServiceImpl.class);
    @Resource
    private GigaSpace gigaSpace;
    private Unmarshaller unmarshaller;

    @Override
    @Transactional
    public void submit(Long id, String post) {
        try {
            Post postEntry = (Post) unmarshaller.unmarshal(new StringReader(post));
            postEntry.setId(id);
            long interval = 7 * 24 * 60 * 60 * 1000;

            if (System.currentTimeMillis() % 2 == 0) {
                // This is done to generate outdated posts
                postEntry.setPublicationDate(new Date(System.currentTimeMillis() - interval * 2));
            } else {
                postEntry.setPublicationDate(new Date());
            }
            gigaSpace.write(postEntry);
        } catch (Exception e) {
            log.error(e,e);
        }
    }

    public void init() throws JAXBException {
        unmarshaller = JAXBContext.newInstance(Post.class).createUnmarshaller();
    }
}
