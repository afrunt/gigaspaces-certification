package com.afrunt.gs.remote.service;

import com.afrunt.gs.model.Post;
import com.afrunt.gs.service.RemotePostService;
import com.j_spaces.core.client.SQLQuery;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openspaces.core.GigaSpace;
import org.openspaces.remoting.RemotingService;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author Andrey Frunt
 */
@RemotingService
public class RemotePostServiceImpl implements RemotePostService {
    private final Log log = LogFactory.getLog(RemotePostServiceImpl.class);
    @Resource
    private GigaSpace gigaSpace;
    private SQLQuery<Post> top10Query = new SQLQuery<Post>(Post.class, "ORDER BY publicationDate DESC");
    private SQLQuery<Post> top10ByUserNameQuery = new SQLQuery<Post>(Post.class, "username=? ORDER BY publicationDate DESC");
    // RLIKE is faster than LIKE
    private SQLQuery<Post> top10ByKeyword = new SQLQuery<Post>(Post.class, "text rlike ? ORDER BY publicationDate DESC");

    private SQLQuery<Post> clearQuery = new SQLQuery<Post>(Post.class, "publicationDate < ?");

    @Override
    public Post[] findTop10Posts() {
        return gigaSpace.readMultiple(top10Query, 10);
    }

    @Override
    public Post[] findTop10PostsByUsername(String username) {
        return gigaSpace.readMultiple(top10ByUserNameQuery.setParameter(1, username), 10);
    }

    @Override
    public Post[] findTop10PostsByKeyword(String keyword) {
        String expression = buildExpression(keyword);
        log.info("Search by [keyword=" + keyword + "][expression=" + expression + "]");
        return gigaSpace.readMultiple(top10ByKeyword.setParameter(1, expression), 10);
    }

    @Override
    public void removeOutdatedPosts() {
        long interval = 7 * 24 * 60 * 60 * 1000; // week in ms
        Post emptyTemplate = new Post();

        int countBefore = gigaSpace.count(emptyTemplate);

        gigaSpace.clear(clearQuery.setParameter(1, new Date(System.currentTimeMillis() - interval)));

        int countAfter = gigaSpace.count(emptyTemplate);

        log.info("Posts cleared [before:" + countBefore + ",after:" + countAfter + "]");
        //System.out.println("Posts cleared [before:" + countBefore + ",after:" + countAfter + "]");
    }

    /**
     * Build regular expression for RLIKE operator
     *
     * @param keyword search keyword
     * @return regular expression for RLIKE operator
     */
    private String buildExpression(String keyword) {
        keyword = quote(keyword);

        if (keyword.contains("*")) {
            StringBuilder expression = new StringBuilder(".*.");

            String[] parts = keyword.split("\\*");
            boolean firsPart = true;
            for (String part : parts) {
                if (firsPart) {
                    firsPart = false;
                } else {
                    expression.append("*");
                }
                expression.append("(").append(part).append(")");
            }
            expression.append(".*");
            return expression.toString();
        } else {
            return ".*.(" + keyword + ").*";
        }
    }

    /**
     * Quote special characters of regular expressions in keyword
     *
     * @param keyword searching keyword
     * @return quoted keyword
     */
    private String quote(String keyword) {
        String specialCharacters = "[\\^$.|?+()";
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < keyword.length(); i++) {
            CharSequence s = keyword.subSequence(i, i + 1);
            if (specialCharacters.contains(s)) {
                result.append("\\");
            }
            result.append(s);
        }

        return result.toString();
    }
}
